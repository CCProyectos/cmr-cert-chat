<%!
public String mask_html (String strIn)
{
	String strOut = "";
	int i;
	char ch;
	if (strIn != null)
	{
		for (i=0; i < strIn.length(); i++)
		{
			ch = strIn.charAt(i);
			if (ch == '<')
				strOut += "<";
			else if (ch == '>')
				strOut += ">";
			else if (ch == '\r')
				strOut += " ";
			else if (ch == '\n')
				strOut += " ";
			else if (ch == '"')
				strOut += "&quot;";
			else if (ch == '&')
				strOut += "&";
			else
				strOut += ch;
		}
	}
	return strOut;
};

public String mask_html_for_textarea (String strIn)
{
	String strOut = "";
	int i;
	char ch;
	if (strIn != null)
	{
		for (i=0; i < strIn.length(); i++)
		{
			ch = strIn.charAt(i);
			if (ch == '<')
				strOut += "&lt;";
			else if (ch == '>')
				strOut += "&gt;";
			else if (ch == '"')
				strOut += "&quot;";
			else if (ch == '&')
				strOut += "&amp;";
			else
				strOut += ch;
		}
	}
	return strOut;
};

public String mask_html (int iIn)
{
	return mask_html(java.lang.Integer.toString (iIn));
}
%>
